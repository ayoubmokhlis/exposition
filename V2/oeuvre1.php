<!DOCTYPE html>
<html lang="en">
  <?php session_start() ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>STANLEY - Free Bootstrap Theme </title>

  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">


  <!-- Custom styles for this template -->
  <link href="assets/css/main.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="assets/js/hover.zoom.js"></script>
  <script src="assets/js/hover.zoom.conf.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

  <!-- Static navbar -->
  <div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">ACCEUIL</a>
      </div>
      <?php if(isset($_SESSION['login'])){?>
                <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="admin_visiteurs.php">Visiteur</a></li>
                    <li><a href="admin_accueil.php">Mon profil </a></li>
                    <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                    </div>

                    <?php }else{?>
                      <div class="navbar-collapse collapse">
                     <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="inscrition.php">Inscription</a></li>
                    <li><a href="session.php">Connexion</a></li>
                    

                        <?php } ?>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>


  <!-- +++++ Projects Section +++++ -->
  <?php
  $mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
  if ($mysqli->connect_errno) {
    // Affichage d'un message d'erreur
    echo "Error: Problème de connexion à la BDD \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    // Arrêt du chargement de la page
    exit();
  }

  // Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
  if (!$mysqli->set_charset("utf8")) {
    printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
    exit();
  } ?>
  <?php
  if ($_GET["id"] != NULL){
  $id = htmlspecialchars(addslashes(($_GET["id"])));
  $req = "SELECT * 
          FROM t_oeuvre_ovr JOIN t_presentation_pre using (ovr_id) JOIN t_exposant_exp using (exp_id) 
          where ovr_id = '$id' ;";
  $result = $mysqli->query($req);
  $ovr = $result->fetch_assoc();

  $req1 = "SELECT * 
          FROM t_oeuvre_ovr JOIN t_presentation_pre using (ovr_id) JOIN t_exposant_exp using (exp_id) 
          where ovr_id = '$id' ;";
  $result1 = $mysqli->query($req1);
  
?>



 <div class="container pt">
    <div class="row mt">
      <div class="col-lg-6 col-lg-offset-3 centered">
        <h3><?php echo $ovr['ovr_intitule'] ?></h3>
        <br>
        <hr>
        <br>

        <p><img class="img-responsive-oeuvre" src="<?php echo $ovr['ovr_fichier_img'];   ?>" alt=""></p>
		    
        <h5><?php echo $ovr['ovr_date_creation'] ?></h5>
        <p><?php echo $ovr['ovr_description'] ?></p>
      </div>
    </div>
    <div class="row mt centered">
      <div class="col-lg-6 col-lg-offset-3 centered">
        
        <h3>Exposant</h3>
           
           <?php while($ovr1 = $result1->fetch_assoc()){ ?>
            <a href="exp.php?ide=<?php echo $ovr1['exp_id'] ?>">
              <h5><?php echo $ovr1['exp_nom'];}?></h5>
          <?php } ?>
                      
              
        

      </div>
    </div><!-- /row -->
  </div><!-- /container -->


 



  <div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
				<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					
					$requete = "SELECT * FROM t_configuration_conf;";
					
					$result1 = $mysqli->query($requete);
					 $actu = $result1->fetch_assoc() ?>

					<h4>L'adresse de l'exposition :</h4>
					<p style="color:#5ad8d8 ">
					<?php echo $actu['conf_lieu']; ?>
					<br>
					lgdefootball@gmail.com	
					</p>
				</div><!-- /col-lg-4 -->
				

				<div class="col-lg-4">
					<h4>Les Dates :</h4>
					
					<h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>
					
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_debut']; ?>

					</p>
					<h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_fin']; ?>
					</p>
					<h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_presentation']; ?>
					</p>
					
					
					
				</div><!-- /col-lg-4 -->

				<div class="col-lg-4">
				<h4><?php echo $actu['conf_intitule']; ?></h4>
				
					<p style="color:#5ad8d8 ">
					
					<?php echo $actu['conf_presentaion']; ?>
					
					</p>
          <?php
						
						$mysqli->close();
						?>

				</div><!-- /col-lg-4 -->

			</div>

		</div>
	</div>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>