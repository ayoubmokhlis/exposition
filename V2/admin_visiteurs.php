<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>STANLEY - Free Bootstrap Theme </title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ACCEUIL</a>
            </div>
            <?php if(isset($_SESSION['login'])){?>
                <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="admin_visiteurs.php">Visiteur</a></li>
                    <li><a href="admin_accueil.php">Mon Profil </a></li>
                    <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                    </div>

                    <?php }else{?>
                      <div class="navbar-collapse collapse">
                     <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="inscrition.php">Inscription</a></li>
                    <li><a href="session.php">Connexion</a></li>
                    

                        <?php } ?>
        </ul>
      </div>
            <!--/.nav-collapse -->
        </div>
    </div>
    

    <!-- +++++ Posts Lists +++++ -->
    <!-- +++++ First Post +++++ -->
    <div id="grey">
		
		
                <?php
                $mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
                if ($mysqli->connect_errno) {
                    // Affichage d'un message d'erreur
                    echo "Error: Problème de connexion à la BDD \n";
                    echo "Errno: " . $mysqli->connect_errno . "\n";
                    echo "Error: " . $mysqli->connect_error . "\n";
                    // Arrêt du chargement de la page
                    exit();
                }

                // Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
                if (!$mysqli->set_charset("utf8")) {
                    printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
                    exit();
                }

                //Préparation de la requête récupérant tous les profils
                $requete = "SELECT * FROM t_visiteur_vis left outer join t_commentaire_com using(vis_id)  ;";
                //Affichage de la requête préparée
                

                $result1 = $mysqli->query($requete);
                if ($result1 == false) //Erreur lors de l’exécution de la requête
                { // La requête a echoué
                    echo "Error: La requête a echoué \n";
                    echo "Errno: " . $mysqli->errno . "\n";
                    echo "Error: " . $mysqli->error . "\n";
                    exit();
                } ?>
                <?php
                if (isset($_GET['reg_err'])) {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch ($err) {
                        case 'success':
                ?>
                            <div class="alert alert-success">
                                <strong>Succès</strong> votre ticket est généré!
                            </div>
                        <?php
                            break;

                        case 'pssword':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> votre mot de passe doit dépasser 15 caractères !
                            </div>
                <?php
                            break;
                    }
                }
                ?>


                <table>
                    <tr>
                        <th>
                            <h4> num de visiteur </h4>
                        </th>
                        <th>
                            <h4> nom du visiteur</h4>
                        </th>
                        <th>
                            <h4> prenom du visiteur</h4>
                        </th>
                        <th>
                            <h4> mot de passe de visiteur</h4>
                        </th>
                        <th>
                            <h4> Date De visite du visiteur</h4>
                        </th>
                        <th>
                            <h4> email de visiteur </h4>
                        </th>
                        <th>
                            <h4>pseudo de createur </h4>
                        </th>
                        <th>
                            <h4> numéro de commentaire </h4>
                        </th>
                        <th>
                            <h4> le commentaire </h4>
                        </th>
                        <th>
                            <h4> date du commentaire </h4>
                        </th>

                    </tr>

                    <?php
                    while ($actu = $result1->fetch_assoc()) {
                    ?>

                        <tr>
                            <th><?php echo $actu['vis_id']; ?></th>
                            <th><?php echo $actu['vis_nom']; ?></th>
                            <th><?php echo $actu['vis_prenom']; ?></th>
                            <th><?php echo $actu['vis_mot_de_passe']; ?></th>
                            <th><?php echo $actu['vis_date_heure']; ?></th>
                            <th><?php echo $actu['vis_email']; ?></th>
                            <th><?php echo $actu['cpt_pseudo']; ?></th>
                            <th><?php echo $actu['com_id']; ?></th>
                            <th><?php echo $actu['com_text']; ?></th>
                            <th><?php echo $actu['com_date_heure']; ?></th>


                        </tr>





                    <?php
                    }
                    $mysqli->close();
                    ?>

                </table>
                <br><br>
                <div class="row mt">
                      <div class="col-lg-6 col-lg-offset-3 centered">
					   <div id="contenu">
                
                            <form role="form" action="action_visiteurs.php" method="post">
                                 
                                <input type="name" class="form-control" name="nticket" placeholder="mot de passe" style="width: 400px;">
                                <button type="submit" class="btn btn-success">generer un nouveau ticket </button>
                            
                            <br><br>

                            
                                <input type="name" class="form-control" name="visiteur" placeholder="numero de vsisiteur" style="width: 400px;">
                                <button type="submit" class="btn btn-success">Supprimer</button>
                            </form>
                            </div>
                            </div>
                            </div>
                        
            </div>
            
</body>

</html>

<!-- +++++ Second Post +++++ -->


<!-- +++++ Third Post +++++ -->




<!-- +++++ Footer Section +++++ -->

<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
				<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					
					$requete = "SELECT * FROM t_configuration_conf;";
					
					$result1 = $mysqli->query($requete);
					 $actu = $result1->fetch_assoc() ?>

					<h4>L'adresse de l'exposition :</h4>
					<p style="color:#5ad8d8 ">
					<?php echo $actu['conf_lieu']; ?>
					<br>
					lgdefootball@gmail.com	
					</p>
				</div><!-- /col-lg-4 -->
				

				<div class="col-lg-4">
					<h4>Les Dates :</h4>
					
					<h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>
					
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_debut']; ?>

					</p>
					<h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_fin']; ?>
					</p>
					<h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_presentation']; ?>
					</p>
					
					
					
				</div><!-- /col-lg-4 -->

				<div class="col-lg-4">
				<h4><?php echo $actu['conf_intitule']; ?></h4>
				
					<p style="color:#5ad8d8 ">
					
					<?php echo $actu['conf_presentaion']; ?>
					
					</p>
          <?php
						
						$mysqli->close();
						?>

				</div><!-- /col-lg-4 -->

			</div>

		</div>
	</div>
	



<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/bootstrap.min.js"></script>
</body>

</html>