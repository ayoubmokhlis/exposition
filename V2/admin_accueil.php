

<?php
/* Vérification ci-dessous à faire sur toutes les pages dont l'accès est
autorisé à un utilisateur connecté. */
session_start();
if (!isset($_SESSION['login'])) //A COMPLETER pour tester aussi le rôle...
{
    //Si la session n'est pas ouverte, redirection vers la page du formulaire
    header("Location:session.php");
}
?>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>STANLEY - Free Bootstrap Theme </title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ACCEUIL</a>
            </div>
            
            <?php if(isset($_SESSION['login'])){?>
                <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="admin_visiteurs.php">Visiteur</a></li>
                    <li><a href="admin_accueil.php">Mon profil </a></li>
                    <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                    </div>

                    <?php }else{?>
                      <div class="navbar-collapse collapse">
                     <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="inscrition.php">Inscription</a></li>
                    <li><a href="session.php">Connexion</a></li>
                    

                        <?php } ?>
        </ul>
      </div>
            <!--/.nav-collapse -->
        </div>
    </div>
    <div id="grey">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
                <?php
			if (isset($_GET['reg_err'])) {
				$err = htmlspecialchars($_GET['reg_err']);

				switch ($err) {
					case 'success':
			?>
						<div class="alert alert-success">
							<strong>Succès</strong> modification reussi!
						</div>
					<?php
						break;

					case 'mdp':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Vos mots de passes ne correspondent pas !
						</div>
					<?php
						break;

					case 'err':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Faut remplir les deux champs !
						</div>
					<?php
						break;

					
				}
			}
			?>

					<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					if ($mysqli->connect_errno) {
						// Affichage d'un message d'erreur
						echo "Error: Problème de connexion à la BDD \n";
						echo "Errno: " . $mysqli->connect_errno . "\n";
						echo "Error: " . $mysqli->connect_error . "\n";
						// Arrêt du chargement de la page
						exit();
					}
					

					// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
					if (!$mysqli->set_charset("utf8")) {
						printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
						exit();
					}
                    $id = $_SESSION['login'];
                    $role = $_SESSION['role'];

					//Préparation de la requête récupérant tous les profils
					$requete=" SELECT * FROM t_profil_pfl JOIN t_compte_cpt  USING (cpt_pseudo) WHERE cpt_pseudo = '" . $id . "' ";
					$result1 = $mysqli->query($requete);
                    $tab1 = $result1->fetch_assoc();


                    $requete1=" SELECT * FROM t_profil_pfl JOIN t_compte_cpt  USING (cpt_pseudo) WHERE cpt_pseudo != '" . $id . "' ";
					$result2 = $mysqli->query($requete1);


					if ($result1 == false || $result2 == false) //Erreur lors de l’exécution de la requête
					{ // La requête a echoué
						echo "Error: La requête a echoué \n";
						echo "Errno: " . $mysqli->errno . "\n";
						echo "Error: " . $mysqli->error . "\n";
						exit();
					} ?>

                    


					<table>
						<tr>
							<th>
								<h4>Nom </h4>
							</th>
							<th>
								<h4>prenom</h4>
                            </th>
							<th>
								<h4>pseudo</h4>
                            </th>
							<th>
								<h4>mail</h4>
                            </th>
                            <th>
								<h4>Role</h4>
                            </th>
                            <th>
								<h4>Validite</h4>
                            </th>
                            <th>
								<h4>Date de creation</h4>
                            </th>
						</tr>

						

							<tr>
								<th><?php echo $tab1['pfl_nom']; ?></th>
								<th><?php echo $tab1['pfl_prenom']; ?></th>
								<th><?php echo $tab1['cpt_pseudo']; ?></th>
								<th><?php echo $tab1['pfl_email']; ?></th>
                                <th><?php echo $tab1['pfl_role']; ?></th>
                                <th><?php echo $tab1['pfl_validite']; ?></th>
                                <th><?php echo $tab1['pfl_date']; ?></th>
							</tr>





						
					</table>
                    <br>
                    
                    <?php  if ($role == 'A'){ ?>
                        
                    <table>
                    <tr>
							<th>
								<h4>Nom </h4>
							</th>
							<th>
								<h4>prenom</h4>
                            </th>
							<th>
								<h4>pseudo</h4>
                            </th>
							<th>
								<h4>mail</h4>
                            </th>
                            <th>
								<h4>Role</h4>
                            </th>
                            <th>
								<h4>Validite</h4>
                            </th>
                            <th>
								<h4>Date de creation</h4>
                            </th>
                            
						</tr>
                        
                        
                        
                        
						<?php
						while ($tab2 = $result2->fetch_assoc()) {
						?>

							<tr>
                                <th><?php echo $tab2['pfl_nom']; ?></th>
								<th><?php echo $tab2['pfl_prenom']; ?></th>
								<th><?php echo $tab2['cpt_pseudo']; ?></th>
								<th><?php echo $tab2['pfl_email']; ?></th>
                                <th><?php echo $tab2['pfl_role']; ?></th>
                                <th><?php echo $tab2['pfl_validite']; ?></th>
                                <th><?php echo $tab2['pfl_date'];} ?></th>
                                
                       
							</tr>



                        </table>
                        <div class="row mt">
                      <div class="col-lg-6 col-lg-offset-3 centered">
					   <div id="contenu">

                    
                         <h3>(ACTIVATION / DESACTIVATION) COMPTES</h3>
                         <hr>
                         <form role="form" action="comptes_action.php" method="post">

                        
                         <input type="name" class="form-control" name="Pseudo" placeholder="pseudo" style="width: 400px;"> <button  class="btn btn-success">ACTIVER / DESACTIVER</button>
                         
                         </form>
                         </div>
                         </div>
                         </div>
					
                    <?php
						}

					?>
                     <div class="row mt">
                      <div class="col-lg-6 col-lg-offset-3 centered">
					   <div id="contenu">

                        
                        <br><br>
                        <form role="form" action="comptes_action.php" method="post">
                        <h3>MODIFICATION DE DONNES PERSONNELLES</h3>
                        <hr>
                        <h5> Nom:</h5>
                         <input type="name" class="form-control" name="Nom" placeholder="nom" style="width: 400px;"> <button  class="btn btn-success">MODIFIER</button>
                         <br><br>
                         
                        <br><br>
                        <h5> Prenom:</h5>
                         <input type="name" class="form-control" name="Prenom" placeholder="preom" style="width: 400px;"> <button  class="btn btn-success">MODIFIER</button>
                         <br><br>
                    
                        <br><br>
                        <h5>Mot de passe :</h5>
                        
                         <input type="name" class="form-control" name="mdp" placeholder="nouveau mot de passe " style="width: 400px;"> 
                         <br>
                        <input type="name" class="form-control" name="cfmdp" placeholder="confirmation de mot de passe " style="width: 400px;"> <button  class="btn btn-success">MODIFIER</button>
                         <br><br>
                         
                         
                        <br><br>
                         </form>
                         </div>
                         </div>
                         </div>
					
                    
                    
                    


                    
                    
                    
                    
									
                    
				</div>

			</div><!-- /row -->
		</div> <!-- /container -->
	</div><!-- /white -->
    <!-- +++++ Footer Section +++++ -->

    <div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
				<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					
					$requete = "SELECT * FROM t_configuration_conf;";
					
					$result1 = $mysqli->query($requete);
					 $actu = $result1->fetch_assoc() ?>

					<h4>L'adresse de l'exposition :</h4>
					<p style="color:#5ad8d8 ">
					<?php echo $actu['conf_lieu']; ?>
					<br>
					lgdefootball@gmail.com	
					</p>
				</div><!-- /col-lg-4 -->
				

				<div class="col-lg-4">
					<h4>Les Dates :</h4>
					
					<h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>
					
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_debut']; ?>

					</p>
					<h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_fin']; ?>
					</p>
					<h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_presentation']; ?>
					</p>
					
					
					
				</div><!-- /col-lg-4 -->

				<div class="col-lg-4">
				<h4><?php echo $actu['conf_intitule']; ?></h4>
				
					<p style="color:#5ad8d8 ">
					
					<?php echo $actu['conf_presentaion']; ?>
					
					</p>
          <?php
						
						$mysqli->close();
						?>

				</div><!-- /col-lg-4 -->

			</div>

		</div>
	</div>
	
    


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>