<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

	<title>STANLEY - Free Bootstrap Theme </title>

	<!-- Bootstrap core CSS -->
	<link href="assets/css/bootstrap.css" rel="stylesheet">


	<!-- Custom styles for this template -->
	<link href="assets/css/main.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="assets/js/hover.zoom.js"></script>
	<script src="assets/js/hover.zoom.conf.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<!-- Static navbar -->
	<div class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">ACCEUIL</a>
			</div>
			<?php if(isset($_SESSION['login'])){?>
                <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="admin_visiteurs.php">Visiteur</a></li>
                    <li><a href="admin_accueil.php">Mon profil </a></li>
                    <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                    </div>

                    <?php }else{?>
                      <div class="navbar-collapse collapse">
                     <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="inscrition.php">Inscription</a></li>
                    <li><a href="session.php">Connexion</a></li>
                    

                        <?php } ?>
        </ul>
      </div>
			<!--/.nav-collapse -->
		</div>
	</div>



	<!-- +++++ Contact Section +++++ -->
	<div id="grey">
		<div class="container pt">
			<div class="row mt">
				<div class="col-lg-6 col-lg-offset-3 centered">
					<h3>Inscrivez vous !  </h3>
					<hr>
					<p>veuillez creer votre nouveau compte en remplissant les champs çi dessous .</p>
				</div>
			</div>
			<?php
			if (isset($_GET['reg_err'])) {
				$err = htmlspecialchars($_GET['reg_err']);

				switch ($err) {
					case 'success':
			?>
						<div class="alert alert-success">
							<strong>Succès</strong> inscription réussie !
						</div>
					<?php
						break;

					case 'password':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Vos mots de passes ne correspondent pas !
						</div>
					<?php
						break;

					case 'email_utilise':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Adresse mail déjà utilisée !
						</div>
					<?php
						break;

					case 'email_validite':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Votre adresse mail n'est pas valide !
						</div>
					<?php
						break;
			
					case 'pseudo':
							?>
								<div class="alert alert-danger">
									<strong>Erreur</strong> Votre pseudo est déjà utilisé !
								</div>
							<?php
						break;

					case 'email_correspondence':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Vos adresses mail ne correspondent pas !
						</div>
					<?php
					break;
					case 'pseudo_length':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Votre pseudo ne doit pas dépasser 255 caractères !
						</div>
					<?php
					break;
					case 'already':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Tous les champs doivent être complétés !
						</div>
						

			<?php
               break;
				}
			}
			?>
			<div class="row mt">
				<div class="col-lg-8 col-lg-offset-2">
					<div id="contenu">
						<form role="form" action="action.php" method="post">
							<fieldset>
								<legend>Données personnelles :</legend>
								<div class="form-group">
									<input type="name" class="form-control" name="pseudo" placeholder="Pseudo">
									<br>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="mdp" placeholder="Mot de passe">
									<br>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="cfmdp" placeholder="Confimez mot de passe">
									<br>
								</div>
								<div class="form-group">
									<input type="name" class="form-control" name="nom" placeholder="Nom">
									<br>
								</div>
								<div class="form-group">
									<input type="name" class="form-control" name="prenom" placeholder="Prénom">
									<br>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Entre email">
									<br>
								</div>

								<button type="submit" class="btn btn-success">S'inscrire</button>
							</fieldset>
						</form>
					</div>
				</div>
			</div><!-- /row -->

		</div><!-- /container -->
	</div>




	<!-- +++++ Footer Section +++++ -->

	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
				<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					
					$requete = "SELECT * FROM t_configuration_conf;";
					
					$result1 = $mysqli->query($requete);
					 $actu = $result1->fetch_assoc() ?>

					<h4>L'adresse de l'exposition :</h4>
					<p style="color:#5ad8d8 ">
					<?php echo $actu['conf_lieu']; ?>
					<br>
					lgdefootball@gmail.com	
					</p>
				</div><!-- /col-lg-4 -->
				

				<div class="col-lg-4">
					<h4>Les Dates :</h4>
					
					<h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>
					
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_debut']; ?>

					</p>
					<h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_fin']; ?>
					</p>
					<h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_presentation']; ?>
					</p>
					
					
					
				</div><!-- /col-lg-4 -->

				<div class="col-lg-4">
				<h4><?php echo $actu['conf_intitule']; ?></h4>
				
					<p style="color:#5ad8d8 ">
					
					<?php echo $actu['conf_presentaion']; ?>
					
					</p>
          <?php
						
						$mysqli->close();
						?>

				</div><!-- /col-lg-4 -->

			</div>

		</div>
	</div>
	



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/bootstrap.min.js"></script>
</body>

</html>