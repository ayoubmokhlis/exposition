<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>


<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

  <title>STANLEY - Free Bootstrap Theme </title>

  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">


  <!-- Custom styles for this template -->
  <link href="assets/css/main.css" rel="stylesheet">

  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

  <!-- Static navbar -->
  <div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">ACCEUIL</a>
      </div>
      <?php if(isset($_SESSION['login'])){?>
                <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="admin_visiteurs.php">Visiteur</a></li>
                    <li><a href="admin_accueil.php">Mon profil </a></li>
                    <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                    </div>

                    <?php }else{?>
                      <div class="navbar-collapse collapse">
                     <ul class="nav navbar-nav navbar-right">
                    <li><a href="galerie1.php">Galerie</a></li>
                    <li><a href="livredor.php">Livre D'or</a></li>
                    <li><a href="inscrition.php">Inscription</a></li>
                    <li><a href="session.php">Connexion</a></li>
                    

                        <?php } ?>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>

  <div class="row mt">
    <div class="col-lg-8 col-lg-offset-2">
      <div id="contenu">
        <?php
        $mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
        if ($mysqli->connect_errno) {
          // Affichage d'un message d'erreur
          echo "Error: Problème de connexion à la BDD \n";
          echo "Errno: " . $mysqli->connect_errno . "\n";
          echo "Error: " . $mysqli->connect_error . "\n";
          // Arrêt du chargement de la page
          exit();
        }

        // Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
        if (!$mysqli->set_charset("utf8")) {
          printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
          exit();
        }

        //Préparation de la requête récupérant tous les profils
        $requete = "SELECT * FROM t_commentaire_com join t_visiteur_vis using(vis_id)  ;";
        //Affichage de la requête préparée
        

        $result1 = $mysqli->query($requete);
        if ($result1 == false) //Erreur lors de l’exécution de la requête
        { // La requête a echoué
          echo "Error: La requête a echoué \n";
          echo "Errno: " . $mysqli->errno . "\n";
          echo "Error: " . $mysqli->error . "\n";
          exit();
        } ?>


        <table>
          <tr>
            <th>
              <h4>nom du visiteur</h4>
            </th>
            <th>
              <h4>prenom du visiteur</h4>
            </th>
            <th>
              <h4>Date Du Commentaire</h4>
            </th>
            <th>
              <h4>Commentaire</h4>
            </th>
          </tr>

          <?php
          while ($actu = $result1->fetch_assoc()) {
          ?>

            <tr>
              <th><?php echo $actu['vis_nom']; ?></th>

              <th><?php echo $actu['vis_prenom']; ?></th>
              <th><?php echo $actu['com_date_heure']; ?></th>
              <th><?php echo $actu['com_text']; ?></th>
            </tr>





          <?php
          }
          $mysqli->close();
          ?>


        </table>
        <br>
                <br>
        <form role="form" action="livredor_action.php" method="post">

        <?php
			if (isset($_GET['reg_err'])) {
				$err = htmlspecialchars($_GET['reg_err']);

				switch ($err) {
					case 'mdpid':
			?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> le numéro de ticket ou le mot de passe est incorrectes
						</div>
					<?php
						break;

					case 'time':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> le visiteur a depassé 3 heures 
						</div>
					<?php
						break;

					case 'commentaire':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> le visiteur a déja laisser un Commentaire
						</div>
					<?php
						break;

					?>
						
					<?php
					case 'success':
					?>
						<div class="alert alert-success">
							<strong>Succès</strong> commentaire ajouter 
						</div>
            <?php
						break;
          	?>
            <?php
					case 'lenght':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> password minimum 15 caractéres
						</div>
            <?php
						break;
          	?>
					<?php
					case 'noremplie':
					?>
						<div class="alert alert-danger">
							<strong>Erreur</strong> Tous les champs doivent être complétés !
						</div>
            <?php
						break;
          	?>
						

			<?php

				}
			}
			?>
							<fieldset>
                
								<legend>Données visiteur :</legend>
								<div class="form-group">
									<input type="name" class="form-control" name="numticket" placeholder="numero de ticket">
									<br>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="mdp" placeholder="Mot de passe">
									<br>
								</div>
								<div class="form-group">
									<input type="name" class="form-control" name="nom" placeholder="Nom visiteur ">
									<br>
								</div>
								<div class="form-group">
									<input type="name" class="form-control" name="prenom" placeholder="Prénom visiteur">
									<br>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="email visiteur">
									<br>
								</div>
                <div class="form-group">
                <textarea  cols="50" rows="10" name="msg" placeholder="commentaire "></textarea>
									<br>
								</div>

								<button type="submit" class="btn btn-success">Ajouter  </button>
							</fieldset>
						</form>
      </div>
    </div>
  </div>
</body>

</html>

<!-- +++++ Second Post +++++ -->


<!-- +++++ Third Post +++++ -->




<!-- +++++ Footer Section +++++ -->

<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
				<?php
					$mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
					
					$requete = "SELECT * FROM t_configuration_conf;";
					
					$result1 = $mysqli->query($requete);
					 $actu = $result1->fetch_assoc() ?>

					<h4>L'adresse de l'exposition :</h4>
					<p style="color:#5ad8d8 ">
					<?php echo $actu['conf_lieu']; ?>
					<br>
					lgdefootball@gmail.com	
					</p>
				</div><!-- /col-lg-4 -->
				

				<div class="col-lg-4">
					<h4>Les Dates :</h4>
					
					<h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>
					
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_debut']; ?>

					</p>
					<h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_fin']; ?>
					</p>
					<h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
					<p style="color:#117864 ">
					<?php echo $actu['conf_date_presentation']; ?>
					</p>
					
					
					
				</div><!-- /col-lg-4 -->

				<div class="col-lg-4">
				<h4><?php echo $actu['conf_intitule']; ?></h4>
				
					<p style="color:#5ad8d8 ">
					
					<?php echo $actu['conf_presentaion']; ?>
					
					</p>
          <?php
						
						$mysqli->close();
						?>

				</div><!-- /col-lg-4 -->

			</div>

		</div>
	</div>
	



<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/bootstrap.min.js"></script>
</body>

</html>