<!DOCTYPE html>
<html lang="en">
<?php session_start() ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>STANLEY - Free Bootstrap Theme </title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/hover.zoom.js"></script>
    <script src="assets/js/hover.zoom.conf.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ACCEUIL</a>
            </div>
            <?php if (isset($_SESSION['login'])) { ?>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="galerie1.php">Galerie</a></li>
                        <li><a href="livredor.php">Livre D'or</a></li>
                        <li><a href="admin_visiteurs.php">Visiteur</a></li>
                        <li><a href="admin_accueil.php">Mon profil </a></li>
                        <li><a href="deconnexion.php">deconnexion</a></li>
                    </ul>
                </div>

            <?php } else { ?>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="galerie1.php">Galerie</a></li>
                        <li><a href="livredor.php">Livre D'or</a></li>
                        <li><a href="inscrition.php">Inscription</a></li>
                        <li><a href="session.php">Connexion</a></li>


                    <?php } ?>
                    </ul>
                </div>
                <!--/.nav-collapse -->
        </div>
    </div>

    <div class="container pt">
        <div class="row mt">
            <div class="col-lg-6 col-lg-offset-3 centered">
                <h3> Les Œuvres </h3>
                <hr>
                <p>Les Œuvres presentent les équipes des Exposants </p>


            </div>
        </div>
        <?php
        $mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');
        if ($mysqli->connect_errno) {
            // Affichage d'un message d'erreur
            echo "Error: Problème de connexion à la BDD \n";
            echo "Errno: " . $mysqli->connect_errno . "\n";
            echo "Error: " . $mysqli->connect_error . "\n";
            // Arrêt du chargement de la page
            exit();
        }

        // Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
        if (!$mysqli->set_charset("utf8")) {
            printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
            exit();
        }

        //Préparation de la requête récupérant tous les profils
        $ovr1 = "SELECT * FROM t_oeuvre_ovr join t_presentation_pre using(ovr_id) group by ovr_id having count(exp_id)=1 ;";
        //Affichage de la requête préparée
        

        $res2 = $mysqli->query($ovr1);
        if ($res2 == false) //Erreur lors de l’exécution de la requête
        { // La requête a echoué
            echo "Error: La requête a echoué \n";
            echo "Errno: " . $mysqli->errno . "\n";
            echo "Error: " . $mysqli->error . "\n";
            exit();
        } ?>
        <div class="row mt centered">
            <?php
            while ($ovr1 = $res2->fetch_assoc()) {
            ?>
                <div class="col-lg-4">
                    <h3><?php echo $ovr1['ovr_intitule']; ?></h3>
                    <a href="oeuvre1.php?id=<?php echo $ovr1['ovr_id'] ?>"><img class="img-responsive-galerie" src="<?php echo $ovr1['ovr_fichier_img'] ?>" alt="" /></a>
                    
                </div>

            <?php } ?>

            <!--/row -->
        </div><!-- /container -->

        <div class="row mt centered">
            <?php
            $req2 = "SELECT * FROM t_oeuvre_ovr join t_presentation_pre using(ovr_id) group by ovr_id having count(exp_id)>=2 ;";
            $res3 = $mysqli->query($req2);
            while ($ovr3 = $res3->fetch_assoc()) {
            ?>
                <div class="col-lg-4">
                    <h3><?php echo $ovr3['ovr_intitule']; ?></h3>
                    <a href="oeuvre1.php?id=<?php echo $ovr3['ovr_id'] ?>"><img class="img-responsive-galerie" src="<?php echo $ovr3['ovr_fichier_img'] ?>" alt="" /></a>
                    
                </div>

            <?php } ?>

            <!--/row -->
        </div><!-- /container -->
        </div>


        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- +++++ Footer Section +++++ -->

        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <?php
                        $mysqli = new mysqli('localhost', 'root', '', 'zfl2-zmokhliay');

                        $requete = "SELECT * FROM t_configuration_conf;";

                        $result1 = $mysqli->query($requete);
                        $actu = $result1->fetch_assoc() ?>

                        <h4>L'adresse de l'exposition :</h4>
                        <p style="color:#5ad8d8 ">
                            <?php echo $actu['conf_lieu']; ?>
                            <br>
                            lgdefootball@gmail.com
                        </p>
                    </div><!-- /col-lg-4 -->


                    <div class="col-lg-4">
                        <h4>Les Dates :</h4>

                        <h5 style="color:#5ad8d8 ">Présentation de l'exposition :</h5>

                        <p style="color:#117864 ">
                            <?php echo $actu['conf_date_debut']; ?>

                        </p>
                        <h5 style="color:#5ad8d8 ">Début de l'exposition:</h5>
                        <p style="color:#117864 ">
                            <?php echo $actu['conf_date_fin']; ?>
                        </p>
                        <h5 style="color:#5ad8d8 ">Fin de l'exposition:</h5>
                        <p style="color:#117864 ">
                            <?php echo $actu['conf_date_presentation']; ?>
                        </p>



                    </div><!-- /col-lg-4 -->

                    <div class="col-lg-4">
                        <h4><?php echo $actu['conf_intitule']; ?></h4>

                        <p style="color:#5ad8d8 ">

                            <?php echo $actu['conf_presentaion']; ?>

                        </p>
                        <?php

                        $mysqli->close();
                        ?>

                    </div><!-- /col-lg-4 -->

                </div>

            </div>
        </div>


</body>

</html>