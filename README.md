# Exposition
Une exposition est un événement ponctuel organisé autour d'un thème (le plus souvent artistique) par un
ou plusieurs responsables (ou commissaires) pour une durée finie (de quelques jours à plusieurs mois)
dans un lieu dédié.
La conception et le développement de l’application d’exposition en ligne s’inscrivent dans le
contexte de la pandémie de ces deux dernières années. Pendant cette période très particulière, des
musées, des lieux d’exposition et des galeries publiques et privées souhaitent disposer d’un outil
permettant de donner des informations dématérialisées au public sur place voire même, le cas
échéant, de rendre accessible en ligne, à distance, l’ensemble des œuvres présentées.

## Nom
Les légendes du football

## Description
L'exposition (gratuite ou non) est ouverte au public aux heures d'ouverture et après obtention d'un ticket
d'entrée (émis par un organisateur / administrateur) sur lequel sont inscrits la date et l'heure de création,
le numéro de visiteur et un mot de passe (et la clé WIFI). Le (ou les) exposant(s) invités présentent une
ou plusieurs œuvres de leur création. Ces œuvres peuvent être individuelles ou collectives. Les œuvres
collectives sont créées spécialement pour l'évènement par plusieurs exposants invités.
Un visiteur doit pouvoir obtenir, s'il le souhaite, des informations complémentaires et détaillées sur
l'exposition, les exposants, les œuvres présentées. Pour visualiser ces données supplémentaires, chaque
visiteur peut emprunter une tablette de l'exposition ou utiliser sa propre tablette ou son smartphone (via
une connexion au réseau local WIFI du lieu). A partir de l'instant d'émission du ticket d'entrée, le
visiteur dispose de 3 heures pour visiter l'exposition et laisser, s'il le souhaite, un commentaire ou une
question sur l'application Web de l'exposition (« livre d'or »). Pour la prise en compte de son
commentaire, le visiteur saisit son numéro de visiteur indiqué sur son ticket et le mot de passe associé et
complète les données qui le concernent (nom, prénom et adresse e-mail).


## Regles de gestion
*******************************Utilisation de l’application****************************
L'application en ligne est accessible aux visiteurs de l’exposition via le réseau local (WIFI) du lieu
d’exposition en temps normal (ou à tous les internautes en cas de reconfinement).


*******************************Utilisateurs de l'application****************************
Il existe deux types d'utilisateur de l'application : les administrateurs ('A') et les organisateurs (ou
commissaires) de l’exposition ('O'). Seuls les titulaires d'un compte utilisateur et d'un profil activé ('A')
peuvent se connecter et accéder à la partie « Back Office » de l'application


*******************************Accès aux informations****************************
Les informations peuvent être visualisées via un ordinateur, un téléphone portable ou une tablette.
L’utilisateur doit pouvoir naviguer dans les différentes rubriques de l’application via son navigateur
Web.



.






