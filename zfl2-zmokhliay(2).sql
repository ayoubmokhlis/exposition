-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mer. 27 avr. 2022 à 01:26
-- Version du serveur :  10.3.9-MariaDB-log
-- Version de PHP :  7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `zfl2-zmokhliay`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_commentaire_com`
--

CREATE TABLE `t_commentaire_com` (
  `com_id` int(11) NOT NULL,
  `com_date_heure` datetime NOT NULL,
  `com_text` varchar(300) NOT NULL,
  `vis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_commentaire_com`
--

INSERT INTO `t_commentaire_com` (`com_id`, `com_date_heure`, `com_text`, `vis_id`) VALUES
(2, '2022-02-21 00:00:00', 'j ai pas trop kiffé', 2),
(3, '2022-02-21 00:00:00', 'suppeer', 3),
(4, '2022-04-25 12:45:27', 'SUPPERR', 10);

-- --------------------------------------------------------

--
-- Structure de la table `t_compte_cpt`
--

CREATE TABLE `t_compte_cpt` (
  `cpt_pseudo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cpt_mot_de_passe` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_compte_cpt`
--

INSERT INTO `t_compte_cpt` (`cpt_pseudo`, `cpt_mot_de_passe`) VALUES
('ali', 'c27bd3cf8a27314a76d0311fbba93721'),
('ayoub', 'c27bd3cf8a27314a76d0311fbba93721'),
('eede', 'c27bd3cf8a27314a76d0311fbba93721'),
('elmoh', 'c27bd3cf8a27314a76d0311fbba93721'),
('emma', 'c27bd3cf8a27314a76d0311fbba93721'),
('fffr', 'c27bd3cf8a27314a76d0311fbba93721'),
('gEstionnaire', '98abb15e560057e55e4e99187702ed4e'),
('hanane', 'c27bd3cf8a27314a76d0311fbba93721'),
('hello', 'c27bd3cf8a27314a76d0311fbba93721'),
('jack', 'c27bd3cf8a27314a76d0311fbba93721'),
('ludivine', 'c27bd3cf8a27314a76d0311fbba93721'),
('mehdi', 'c27bd3cf8a27314a76d0311fbba93721'),
('momo148', '4124bc0a9335c27f086f24ba207a4912'),
('youssef', 'c27bd3cf8a27314a76d0311fbba93721');

-- --------------------------------------------------------

--
-- Structure de la table `t_configuration_conf`
--

CREATE TABLE `t_configuration_conf` (
  `conf_intitule` varchar(80) NOT NULL,
  `conf_date_debut` date NOT NULL,
  `conf_date_fin` date NOT NULL,
  `conf_presentaion` varchar(100) NOT NULL,
  `conf_lieu` varchar(100) NOT NULL,
  `conf_date_presentation` date NOT NULL,
  `conf_text_bienv` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_configuration_conf`
--

INSERT INTO `t_configuration_conf` (`conf_intitule`, `conf_date_debut`, `conf_date_fin`, `conf_presentaion`, `conf_lieu`, `conf_date_presentation`, `conf_text_bienv`) VALUES
('Les Légendes de Football', '2022-04-25', '2022-04-27', 'Sur ce site vous trouverez tout les informations nécessaires sur vos joueurs préférés  ', '25 Rue de Pontaniou, 29200 Brest', '2022-04-25', 'Bienvenu sur notre site , je serai votre référent .  ');

-- --------------------------------------------------------

--
-- Structure de la table `t_exposant_exp`
--

CREATE TABLE `t_exposant_exp` (
  `exp_id` int(11) NOT NULL,
  `exp_nom` varchar(80) NOT NULL,
  `exp_prenom` varchar(80) NOT NULL,
  `exp_text_bio` varchar(100) NOT NULL,
  `exp_email` varchar(100) NOT NULL,
  `exp_url_site_web` varchar(200) NOT NULL,
  `exp_fichier_img` varchar(200) NOT NULL,
  `cpt_pseudo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_exposant_exp`
--

INSERT INTO `t_exposant_exp` (`exp_id`, `exp_nom`, `exp_prenom`, `exp_text_bio`, `exp_email`, `exp_url_site_web`, `exp_fichier_img`, `cpt_pseudo`) VALUES
(1, 'NEYMAR', 'Da Silva', 'un joueur de football brésilien, il est né le 5 février 1992 .', 'neymarjr@gmail.com', 'https://www.neymarjr.com/en', 'assets/img/portfolio/neymar.png', 'gEstionnaire'),
(2, 'CRISTIANO', 'Ronaldo', 'un joueur de football portugais, il est né le 5 février 1985 .', 'ronaldocr7@gmail.com', 'https://www.cristianoronaldo.com/#cr7', 'assets/img/portfolio/ronaldo.png', 'gEstionnaire'),
(3, 'MESSI', 'Lionel', 'Lionel Andrés Messi Cuccitini, né le 24 juin 1987 en Argentine, est un footballeur argentin évoluant', 'messi10@gmail.com', 'https://messi.com/', 'assets/img/portfolio/messi.png', 'gEstionnaire'),
(4, 'KYLIAN', 'Mbappé', 'Kylian Mappé Bottin est né le 20 décembre 1998 dans le 19e arrondissement de Paris. ', 'kylianmp@gmail.com', 'https://kylianmbappe.com/', 'assets/img/portfolio/mbappe.png', 'gEstionnaire'),
(5, 'BENZEMA', 'Karim', 'Karim Benzema est né le 19 décembre 1987 à Lyon.', 'Karimbz@gmail.com', 'https://www.realmadrid.com/fr/football/effectif/karim-benzema', 'assets/img/portfolio/benzema.png', 'gEstionnaire');

-- --------------------------------------------------------

--
-- Structure de la table `t_news_new`
--

CREATE TABLE `t_news_new` (
  `new_id` int(11) NOT NULL,
  `new_titre` varchar(40) NOT NULL,
  `new_texte` varchar(300) NOT NULL,
  `new_date` date NOT NULL,
  `cpt_pseudo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_news_new`
--

INSERT INTO `t_news_new` (`new_id`, `new_titre`, `new_texte`, `new_date`, `cpt_pseudo`) VALUES
(1, 'Ballon dOr : pourquoi les légendes Pelé ', 'En effet, cette distinction n était réservée qu aux seuls joueurs européens jusqu en 1995. Le mode d attribution a ensuite évolué pour s ouvrir à tous les joueurs évoluant dans un club européen puis à toutes les nationalités en 2007.', '2022-01-21', 'gEstionnaire'),
(2, 'rencontre au sommet entre Kylian Mbappé ', 'Le prodige français et le triple Champion du monde brésilien, souvent comparés, ont été réunis pour la première fois, mardi 2 avril.', '2022-04-15', 'gEstionnaire'),
(3, ' Lionel Messi veut déjà quitter le PSG !', 'D après les médias catalans, l attaquant argentin souhaite partir et... revenir au FC Barcelone. ', '2022-05-21', 'gEstionnaire'),
(4, 'Cristiano Ronaldo motive les supporters ', 'Avant la rencontre de Ligue des champions face à l Atlético Madrid, Cristiano Ronaldo est visiblement très motivé. L attaquant de Manchester United a demandé lundi le soutien de ses supporters dans un message posté sur ses réseaux sociaux. ', '2021-02-21', 'gEstionnaire');

-- --------------------------------------------------------

--
-- Structure de la table `t_oeuvre_ovr`
--

CREATE TABLE `t_oeuvre_ovr` (
  `ovr_id` int(11) NOT NULL,
  `ovr_intitule` varchar(80) NOT NULL,
  `ovr_date_creation` date NOT NULL,
  `ovr_description` varchar(300) DEFAULT NULL,
  `ovr_fichier_img` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_oeuvre_ovr`
--

INSERT INTO `t_oeuvre_ovr` (`ovr_id`, `ovr_intitule`, `ovr_date_creation`, `ovr_description`, `ovr_fichier_img`) VALUES
(1, 'Real Madrid', '1902-01-01', 'Le Real Madrid C.F. (Real Madrid Club de Fútbol) est un club de football espagnol. La section football est fondée en 1902', 'assets/img/portfolio/real.png'),
(2, 'Paris St Germain', '1970-08-12', 'Le Paris Saint-Germain Football Club,est un club de football français, basé à Saint-Germain-en-Laye et à Paris.', 'assets/img/portfolio/psg.png'),
(3, ' FC Barcelone ', '1899-11-29', 'Le Futbol Club Barcelona , st un club de football espagnol fondé en 1899, basé à Barcelone, capitale de la Catalogne.', 'assets/img/portfolio/barcelone.png'),
(4, 'AFA', '1893-02-21', 'L\'Association du Football Argentin est la plus ancienne du continent américain et la huitième au monde.', 'assets/img/portfolio/AFA.png'),
(5, 'Fc Monaco', '1919-08-01', 'L\'Association sportive de Monaco Football Club, en abrégé AS Monaco ou ASM, est un club de football fondé en 1924 et situé à Monaco.', 'assets/img/portfolio/monaco.png'),
(6, 'FFF', '1904-01-01', 'L\'équipe de France de football, créée en 1904, est l\'équipe nationale qui représente la France.', 'assets/img/portfolio/france.png'),
(7, 'Juventus FC', '1897-01-01', 'Le Juventus Football Club, dite la Juventus, est un club de football professionnel italien fondé en 1897 à Turin.', 'assets/img/portfolio/juventus.png'),
(8, 'Manchester United Football Club', '1878-01-01', 'Le Manchester United Football Club est un club de football anglais basé dans le district de Trafford, à proximité de la ville de Manchester.', 'assets/img/portfolio/mufc.jpg'),
(9, 'Olympique lyonnais', '1950-01-01', 'L\'Olympique lyonnais est un club de football français fondé en 1950 à Lyon. Le club, basé au Parc Olympique lyonnais à Décines-Charpieu, est présidé depuis juin 1987 par Jean-Michel Aulas.', 'assets/img/portfolio/lyon.png'),
(10, 'santos FC', '1912-04-14', 'e Santos Futebol Clube est un club brésilien de football fondé le 14 avril 1912 et basé dans la ville de Santos dans l\'État de São Paulo.', 'assets/img/portfolio/santos.png');

-- --------------------------------------------------------

--
-- Structure de la table `t_presentation_pre`
--

CREATE TABLE `t_presentation_pre` (
  `exp_id` int(11) NOT NULL,
  `ovr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_presentation_pre`
--

INSERT INTO `t_presentation_pre` (`exp_id`, `ovr_id`) VALUES
(1, 2),
(1, 3),
(1, 10),
(2, 1),
(2, 7),
(2, 8),
(3, 2),
(3, 3),
(3, 4),
(4, 2),
(4, 5),
(4, 6),
(5, 1),
(5, 6),
(5, 9);

-- --------------------------------------------------------

--
-- Structure de la table `t_profil_pfl`
--

CREATE TABLE `t_profil_pfl` (
  `pfl_id` int(11) NOT NULL,
  `pfl_nom` varchar(80) NOT NULL,
  `pfl_prenom` varchar(80) NOT NULL,
  `pfl_email` varchar(100) NOT NULL,
  `pfl_role` char(1) NOT NULL,
  `pfl_validite` char(1) NOT NULL,
  `pfl_date` date NOT NULL,
  `cpt_pseudo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_profil_pfl`
--

INSERT INTO `t_profil_pfl` (`pfl_id`, `pfl_nom`, `pfl_prenom`, `pfl_email`, `pfl_role`, `pfl_validite`, `pfl_date`, `cpt_pseudo`) VALUES
(1, 'nais', 'ibra', 'naisibra@gmail.com', 'A', 'A', '2022-01-26', 'gEstionnaire'),
(2, 'mokhlis', 'ayoub', 'mokhlisseayoub8@gmail.com', 'A', 'A', '2022-02-20', 'ayoub'),
(3, 'amine', 'elmoh', 'amineelmoh@gmail.com', 'A', 'D', '2022-03-26', 'elmoh'),
(4, 'lemoine', 'emma', 'lemoineemma@gmail.com', 'O', 'D', '2022-04-26', 'emma'),
(5, 'bentalia', 'hanane', 'bentaliahanane@gmail.com', 'O', 'D', '2022-04-26', 'hanane'),
(6, 'olivier', 'jack', 'olivierjack@gmail.com', 'O', 'D', '2022-04-26', 'jack'),
(7, 'riviere', 'ludivine', 'riviereludivine@gmail.com', 'O', 'D', '2022-04-26', 'ludivine'),
(8, 'blouqat', 'ali', 'blouqatali@gmail.com', 'O', 'A', '2022-04-26', 'ali'),
(9, 'hajji', 'youssef', 'hajjiyoussef@gmail.com', 'O', 'D', '2022-04-26', 'youssef'),
(10, 'nabil', 'mehdi', 'nabilmehdi@gmail.com', 'O', 'D', '2022-04-26', 'mehdi'),
(23, 'le marc', 'v', 'jfjh@gmail.com', 'O', 'A', '2022-04-25', 'momo148');

-- --------------------------------------------------------

--
-- Structure de la table `t_visiteur_vis`
--

CREATE TABLE `t_visiteur_vis` (
  `vis_id` int(11) NOT NULL,
  `vis_mot_de_passe` char(80) NOT NULL,
  `vis_date_heure` datetime NOT NULL,
  `vis_nom` varchar(80) DEFAULT NULL,
  `vis_prenom` varchar(80) DEFAULT NULL,
  `vis_email` varchar(100) DEFAULT NULL,
  `cpt_pseudo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_visiteur_vis`
--

INSERT INTO `t_visiteur_vis` (`vis_id`, `vis_mot_de_passe`, `vis_date_heure`, `vis_nom`, `vis_prenom`, `vis_email`, `cpt_pseudo`) VALUES
(2, 'c27bd3cf8a27314a76d0311fbba93721', '2022-02-21 00:00:00', 'issa', 'mendy', 'issamendy@gmail.com', 'gEstionnaire'),
(3, 'c27bd3cf8a27314a76d0311fbba93721', '2022-02-21 00:00:00', 'oussama', 'bakhta', 'oussamabakhta@gmail.com', 'gEstionnaire'),
(8, 'c27bd3cf8a27314a76d0311fbba93721', '2022-04-25 11:57:52', NULL, NULL, NULL, 'gEstionnaire   '),
(9, 'c27bd3cf8a27314a76d0311fbba93721', '2022-04-24 00:00:00', NULL, NULL, NULL, 'gEstionnaire'),
(10, 'c27bd3cf8a27314a76d0311fbba93721', '2022-04-25 12:44:01', 'BOB', 'BOBA', 'BOBA@GMAIL.COM', 'momo148');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_commentaire_com`
--
ALTER TABLE `t_commentaire_com`
  ADD PRIMARY KEY (`com_id`),
  ADD UNIQUE KEY `vis_id` (`vis_id`);

--
-- Index pour la table `t_compte_cpt`
--
ALTER TABLE `t_compte_cpt`
  ADD PRIMARY KEY (`cpt_pseudo`);

--
-- Index pour la table `t_configuration_conf`
--
ALTER TABLE `t_configuration_conf`
  ADD PRIMARY KEY (`conf_intitule`);

--
-- Index pour la table `t_exposant_exp`
--
ALTER TABLE `t_exposant_exp`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `cpt_pseudo` (`cpt_pseudo`);

--
-- Index pour la table `t_news_new`
--
ALTER TABLE `t_news_new`
  ADD PRIMARY KEY (`new_id`),
  ADD KEY `cpt_pseudo` (`cpt_pseudo`);

--
-- Index pour la table `t_oeuvre_ovr`
--
ALTER TABLE `t_oeuvre_ovr`
  ADD PRIMARY KEY (`ovr_id`);

--
-- Index pour la table `t_presentation_pre`
--
ALTER TABLE `t_presentation_pre`
  ADD PRIMARY KEY (`exp_id`,`ovr_id`),
  ADD KEY `ovr_id` (`ovr_id`);

--
-- Index pour la table `t_profil_pfl`
--
ALTER TABLE `t_profil_pfl`
  ADD PRIMARY KEY (`pfl_id`),
  ADD KEY `cpt_pseudo` (`cpt_pseudo`);

--
-- Index pour la table `t_visiteur_vis`
--
ALTER TABLE `t_visiteur_vis`
  ADD PRIMARY KEY (`vis_id`),
  ADD KEY `cpt_pseudo` (`cpt_pseudo`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_commentaire_com`
--
ALTER TABLE `t_commentaire_com`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_exposant_exp`
--
ALTER TABLE `t_exposant_exp`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `t_news_new`
--
ALTER TABLE `t_news_new`
  MODIFY `new_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_oeuvre_ovr`
--
ALTER TABLE `t_oeuvre_ovr`
  MODIFY `ovr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `t_presentation_pre`
--
ALTER TABLE `t_presentation_pre`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `t_profil_pfl`
--
ALTER TABLE `t_profil_pfl`
  MODIFY `pfl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `t_visiteur_vis`
--
ALTER TABLE `t_visiteur_vis`
  MODIFY `vis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_commentaire_com`
--
ALTER TABLE `t_commentaire_com`
  ADD CONSTRAINT `t_commentaire_com_ibfk_1` FOREIGN KEY (`vis_id`) REFERENCES `t_visiteur_vis` (`vis_id`);

--
-- Contraintes pour la table `t_exposant_exp`
--
ALTER TABLE `t_exposant_exp`
  ADD CONSTRAINT `t_exposant_exp_ibfk_1` FOREIGN KEY (`cpt_pseudo`) REFERENCES `t_compte_cpt` (`cpt_pseudo`);

--
-- Contraintes pour la table `t_news_new`
--
ALTER TABLE `t_news_new`
  ADD CONSTRAINT `t_news_new_ibfk_1` FOREIGN KEY (`cpt_pseudo`) REFERENCES `t_compte_cpt` (`cpt_pseudo`);

--
-- Contraintes pour la table `t_presentation_pre`
--
ALTER TABLE `t_presentation_pre`
  ADD CONSTRAINT `t_presentation_pre_ibfk_1` FOREIGN KEY (`exp_id`) REFERENCES `t_exposant_exp` (`exp_id`),
  ADD CONSTRAINT `t_presentation_pre_ibfk_2` FOREIGN KEY (`ovr_id`) REFERENCES `t_oeuvre_ovr` (`ovr_id`);

--
-- Contraintes pour la table `t_profil_pfl`
--
ALTER TABLE `t_profil_pfl`
  ADD CONSTRAINT `t_profil_pfl_ibfk_1` FOREIGN KEY (`cpt_pseudo`) REFERENCES `t_compte_cpt` (`cpt_pseudo`);

--
-- Contraintes pour la table `t_visiteur_vis`
--
ALTER TABLE `t_visiteur_vis`
  ADD CONSTRAINT `t_visiteur_vis_ibfk_1` FOREIGN KEY (`cpt_pseudo`) REFERENCES `t_compte_cpt` (`cpt_pseudo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
